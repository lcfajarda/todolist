<?php

namespace App\Http\Controllers;

use App\Todo;

use Illuminate\Http\Request;
use DB;

class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $todos = Todo::all();
        $todos = DB::table('todos')->paginate(5);
        return view('todos.index')->with("todos",$todos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $todo = new Todo;
        $todo->task = $request->input('task');
        $todo->date = $request->input('date');
        $todo->save();

       $todos = Todo::all();
       $todos = DB::table('todos')->paginate(5);
       return view('todos.index')->with('todo',$todo)->with('todos',$todos);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $todo = Todo::find($id);
        return view('todos.edit')->with('todo',$todo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todo $todo)
    {
        $todo = new Todo;
        $todo->task = $request->task; 
        $todo->date = $request->date; 
        $todo->save();

        $todos = Todo::all();
       return view('todos.index')->with('todo',$todo)->with('todos',$todos);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todo = Todo::find($id);
        $todo->delete();

        $todos = Todo::all();
        $todos = DB::table('todos')->paginate(5);
        return view('todos.index')->with('todo',$todo)->with('todos',$todos);
    }
}
