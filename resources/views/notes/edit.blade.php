<!DOCTYPE html>
<html>
@extends('layouts.app')
<head>
	<title></title>
</head>
<body id="notebod">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item ">Note Taking</li>
		    <li class="breadcrumb-item active" ><a href="{{route('todos.index')}}">To Do List</a></li>
		  </ol>
		</nav>
	<div class="container">


			<div class="row">
				  <div class="col-8 mt-5">
				  	<h2 class="addnote">Edit Notes</h2>
				
				<form action="/notes/{{$note->id}}" method="POST">
						@csrf
						@method('PUT')

			  			<div class="form-group">
								<label for="title">Title</label>
								<textarea id="title" class="form-control"  cols="10"  name="title" >{{$note->title}}</textarea>
							</div>
						
						

							<div class="form-group">
								<label for="text">Text</label>
								<textarea id="text" class="form-control" rows="7" cols="10"  name="text" >{{$note->text}}</textarea>
							</div>

					<button class="btn btn-primary btn-block">Update</button>
				</form>

			</div>
		</div>
</body>
</html>