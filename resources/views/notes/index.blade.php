<!DOCTYPE html>
<html>
@extends('layouts.app')
<head>
	<title>Note taking Activity</title>
</head>
<body id="notebod">
		<nav aria-label="breadcrumb" >
		  <ol class="breadcrumb" id="nav">
		  	<h4 class="logo"><strong>Todo it all</strong></h4>
		    <li class="breadcrumb-item notell">Note Taking</li>
		    <li class="breadcrumb-item active notell" ><a href="{{route('todos.index')}}">To Do List</a></li>
		  </ol>
		</nav>

		

		<div class="container" id="contnote">


			<div class="row">

			
				
			 <div class="col-8 mt-5" >
			 	<div class="container" id="colnote">
				  	<h2 class="addnote">Add Note</h2>
				
				<form action="/notes" method="POST">
						@csrf
			  			<div class="form-group">
								<label for="title" class="notel">Title</label>
								<textarea id="title" class="form-control"  cols="10"  name="title" placeholder="Add Title"></textarea>
							</div>
						
							<div class="form-group">
								<label for="text" class="notel">Text</label>
								<textarea id="text" class="form-control" rows="7" cols="10"  name="text" placeholder="Add Text"></textarea>
							</div>
					<button class="notel btn btn-primary btn-block ">Submit</button>
				</form>

			</div>

		</div>

				
				  	<div class="col-4">
						<table class= "table table-dark mt-5" id="tb">
								<thead>
									<th scope="col">Title</th>
									<th scope="col">Text</th>
									<th scope="col"></th>
									<th scope="col">Action</th>
								</thead>
								<tbody>
									@foreach($notes as $note)
								  <tr>	
									<td>{{$note->title}}</td>
									<td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong{{$note->id}}">
									 show
									</button>
										
									</td>
									<td>

										<form action="/notes/{{$note->id}}" method="POST">
											@csrf
											@method('DELETE')
											<button class="btn btn-danger" type="submit" name="submit" value="Delete">Delete</button>
											
										</form>
								</td>
									<td><a href="/notes/{{$note->id}}/edit"><button type="button"  class="btn btn-success">update</button></a></td>
									

								  </tr>


								  	<!-- Modal -->
										<div class="modal fade" id="exampleModalLong{{$note->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
										  <div class="modal-dialog" role="document">
										    <div class="modal-content">
										      <div class="modal-header">
										        <h5 class="modal-title" id="exampleModalLongTitle">{{$note->title}}</h5>
										        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
										          <span aria-hidden="true">&times;</span>
										        </button>
										      </div>
										      <div class="modal-body">
										      {{$note->text}}
										      </div>
										      <div class="modal-footer">
										        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
										       
										      </div>
										    </div>
										  </div>
										</div>

								  @endforeach
								    
										
								</tbody>
						</table>
					<div>
						{{ $notes->links() }}
					</div>
				</div>
			</div>
			
		</div>
</body>
</html>