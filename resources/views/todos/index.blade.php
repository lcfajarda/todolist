<!DOCTYPE html>
<html>
@extends('layouts.app')
<head>
	<title>To Do List</title>
</head>
<body id="notebod">
		<nav aria-label="breadcrumb" >
		  <ol class="breadcrumb" id="nav">
		  	<h4 class="logo"><strong>Todo it all</strong></h4>
		    <li class="breadcrumb-item active notell" id="todofont"><a href="{{route('notes.index')}}">Note Taking</a></li>
		    <li class="breadcrumb-item notell" >To Do List</li>
		  </ol>
		</nav>

		

		<div class="container">
			<div class="row">
				  <div class="col-6 mt-5">
				  	<div class="container" id="coltodo">
				  		<h2 id="addtask">Add Task</h2>
				
							<form action="/todos" method="POST">
									@csrf
						  			<div class="form-group">
											<label for="task" id="todofont">Task</label>
											<textarea id="task" class="form-control"  cols="10"  name="task" placeholder="add new task"></textarea>
										</div>
									
									

										<div class="form-group">
											<label for="task" id="todofont">Add Date and Time</label>
											<br>
											<input type="datetime-local" name="date" id="date">
										</div>

								<button class="btn btn-primary btn-block">Submit</button>
							</form>
						</div>
				</div>




				
				  	<div class="col-6">
						<table class= "table table-dark" id="tb">
								<thead>
									<th scope="col">Task</th>
									<th scope="col">Date and Time</th>
									<th scope="col">Action</th>
									<th scope="col"></th>
									<th scope="col">Is Completed</th>

								</thead>
								<tbody>
								@foreach($todos as $todo)
								
								  <tr>	
									<td>{{$todo->task}}</td>
									<td>{{$todo->date}}</td>
									<td>

										<form action="/todos/{{$todo->id}}" method="POST">
											@csrf
											@method('DELETE')
											<button class="btn btn-danger">Delete</button>
										</form>
								</td>
									<td><a href="/todos/{{$todo->id}}/edit"><button type="button"  class="btn btn-success">Edit</button></a></td>
									<td>  
									{{-- <input type="checkbox" id="myCheck{{$todo->id}}"  onclick="myFunction()"> --}}
  		
									</td>
									
									
    									
    							
								  </tr>

								 @endforeach

								</tbody>
						</table>
		
					<div>
						{{ $todos->links() }}
					</div>

				</div>
			</div>
		</div>


		<script>
function myFunction() {
  document.getElementById("demo").innerHTML = "Hello World";
}
</script>
</body>
</html>